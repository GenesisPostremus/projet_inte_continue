package i1.epsi.webapp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebAppController {

    @GetMapping("/test")
    public String afficherTest(){
        return "test";
    }

}
