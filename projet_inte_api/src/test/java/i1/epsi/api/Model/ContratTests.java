package i1.epsi.api.Model;

import i1.epsi.api.model.Contrat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.stream.Stream;

@RunWith(Parameterized.class)
public class ContratTests {
    static Stream<Arguments> chagerJeuDetest() throws Throwable{
        return Stream.of(
                Arguments.of(10,5,15)
        );
    }

    static Stream<Arguments> chagerJeuDetestBaisser() throws Throwable{
        return Stream.of(
                Arguments.of(10,5,5)
        );
    }

    @ParameterizedTest(name = "Augmentation du Pourcentage")
    @MethodSource("chagerJeuDetest")
    public void testAugmenterPourcentageContrat(Integer pourcentageBase, Integer pourcentageAugment, Integer pourcentageFinale){
        Contrat contrat = new Contrat();
        contrat.setPourcentage(pourcentageBase);

        contrat.augmenterPourcentage(pourcentageAugment);

        Assertions.assertEquals(pourcentageFinale, contrat.getPourcentage());
    }

    @ParameterizedTest(name = "Baisser le Pourcentage")
    @MethodSource("chagerJeuDetestBaisser")
    public void testBaisserPourcentageContrat(Integer pourcentageBase, Integer pourcentageAugment, Integer pourcentageFinale){
        Contrat contrat = new Contrat();
        contrat.setPourcentage(pourcentageBase);

        contrat.baisserPourcentage(pourcentageAugment);

        Assertions.assertEquals(pourcentageFinale, contrat.getPourcentage());
    }
}
