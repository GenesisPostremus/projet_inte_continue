package i1.epsi.api.Model;

import i1.epsi.api.model.Produit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.stream.Stream;

@RunWith(Parameterized.class)
public class ProduitTests {
    static Stream<Arguments> chargerJeuDeTestAugmenterPrix() throws Throwable{
        return Stream.of(
                Arguments.of(10,5,15),
                Arguments.of(5,5,10),
                Arguments.of(2,2,4)
        );
    }

    static Stream<Arguments> chargerJeuDeTestDiminuerPrix() throws Throwable{
        return Stream.of(
                Arguments.of(10,5,5),
                Arguments.of(7,5,2),
                Arguments.of(2,1,1)
        );
    }

    static Stream<Arguments> chagerJeuDetestNom() throws Throwable{
        return Stream.of(
                Arguments.of("Jean","Michel")
        );
    }

    @ParameterizedTest(name = "Augmentation du prix")
    @MethodSource("chargerJeuDeTestAugmenterPrix")
    public void testAugmenterPrixProduit(Integer prixBase, Integer prixAugment, Integer prixFinale){
        Produit produit = new Produit();
        produit.setPrix_produit(prixBase);

        produit.augmenterPrix(prixAugment);

        Assertions.assertEquals(prixFinale, produit.getPrix_produit());
    }
    @ParameterizedTest(name = "Diminuer du prix")
    @MethodSource("chargerJeuDeTestDiminuerPrix")
    public void testDiminuerPrixProduit(Integer prixBase, Integer prixAugment, Integer prixFinale){
        Produit produit = new Produit();
        produit.setPrix_produit(prixBase);

        produit.baisserPrixProduit(prixAugment);

        Assertions.assertEquals(prixFinale, produit.getPrix_produit());
    }

    @ParameterizedTest(name = "Changer le nom d'un produit")
    @MethodSource("chagerJeuDetestNom")
    public void testChangementNomProduit(String ancienNom, String nouveauNom){
        Produit produit = new Produit();
        produit.setNom_produit(ancienNom);

        produit.setNom_produit(nouveauNom);

        Assertions.assertEquals(nouveauNom, produit.getNom_produit());
    }
}
