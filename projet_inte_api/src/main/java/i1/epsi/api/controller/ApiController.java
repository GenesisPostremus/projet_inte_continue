package i1.epsi.api.controller;

import i1.epsi.api.dao.ClientRepository;
import i1.epsi.api.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/api")
public class ApiController {

    @Autowired
    private ClientRepository clientRepository;

    @GetMapping("/test")
    public String index(){
        return "Hello World";
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Client> getAllUsers(){
        return clientRepository.findAll();
    }

    /* @GetMapping(path = "/client/{id}")
    public @ResponseBody Iterable<Client> getClientById(@PathVariable String id) {
        return clientRepository.findAllById(Collections.singleton(Integer.valueOf(id)));
    }*/
}
