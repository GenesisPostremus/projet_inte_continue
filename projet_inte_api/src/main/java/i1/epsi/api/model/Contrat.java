package i1.epsi.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contrat")
public class Contrat {
    @Id
    @Column(name = "id_contrat")
    private Long id;
    private Integer Pourcentage;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Integer getPourcentage() {
        return Pourcentage;
    }

    public void setPourcentage(Integer new_pourcentage) {
        this.Pourcentage = new_pourcentage;
    }

    public void augmenterPourcentage(Integer val){
        this.Pourcentage += val;
    }

    public void baisserPourcentage(Integer val){
        this.Pourcentage -= val;
    }
}
