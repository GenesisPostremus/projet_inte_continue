package i1.epsi.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produit")
public class Produit {
    @Id
    @Column(name = "id_produit")
    private Long id;
    private String nom_produit;
    private Integer prix_produit;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }

    public Integer getPrix_produit() {
        return prix_produit;
    }

    public void setPrix_produit(Integer priw_produit) {
        this.prix_produit = priw_produit;
    }

    public void augmenterPrix(Integer val){
        this.prix_produit += val;
    }

    public void baisserPrixProduit(Integer val){
        this.prix_produit -= val;
    }

    public void changerNomProduit(String nouveauNom){
        this.nom_produit = nouveauNom;
    }
}
