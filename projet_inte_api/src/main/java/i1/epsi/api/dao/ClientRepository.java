package i1.epsi.api.dao;

import org.springframework.data.repository.CrudRepository;
import i1.epsi.api.model.Client;

public interface ClientRepository extends CrudRepository<Client, Integer> {
}