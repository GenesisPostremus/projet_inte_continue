package i1.epsi.api.exception;

public class ClientException extends Throwable {
    public ClientException(String s) {
        super(s);
    }
}
